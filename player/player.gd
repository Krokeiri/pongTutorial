extends KinematicBody2D

var epicfastbarryallenspeed = 750

func _physics_process(delta):
	var epicfastbarryallenvelocity = Vector2.ZERO
	if Input.is_action_pressed("ui_up"):
		epicfastbarryallenvelocity.y -= 1
	if Input.is_action_pressed("ui_down"):
		epicfastbarryallenvelocity.y += 1
	move_and_slide(epicfastbarryallenvelocity * epicfastbarryallenspeed)
