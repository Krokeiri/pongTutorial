extends Node

var playerScore = 0
var opponentScore = 0

func score_achieved():
		$CountdownLabel.visible = true
		$Ball.position = Vector2(640,360)
		get_tree().call_group("BallGroup","stop_ball")
		$CountdownTimer.start()
		$ScoreSound.play()
		$Player.position.x = 128
		$Opponent.position.x = 1152

func _on_End_Zone_Left_body_entered(body):
	score_achieved()
	opponentScore += 1
	
func _on_End_Zone_Right_body_entered(body):
	score_achieved()
	playerScore += 1

func _process(delta):
	$PlayerScore.text = str(playerScore)
	$OpponentScore.text = str(opponentScore)
	$CountdownLabel.text = str(int($CountdownTimer.time_left) + 1)

func _on_CountdownTimer_timeout():
	get_tree().call_group("BallGroup","restart_ball")
	$CountdownLabel.visible = false
