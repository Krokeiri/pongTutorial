extends KinematicBody2D

var ballwhoooshspeed = 600
var ballwhoooshvelocity = Vector2.ZERO

func _ready():
	randomize()
	ballwhoooshvelocity.x = [-1,1][randi() % 2]
	ballwhoooshvelocity.y = [-0.8,0.8][randi() % 2]
	
func _physics_process(delta):
	var collision_object = move_and_collide(ballwhoooshvelocity * ballwhoooshspeed * delta)
	if collision_object:
		ballwhoooshvelocity = ballwhoooshvelocity.bounce(collision_object.normal)
		$CollisionSound.play()
		
func stop_ball():
	ballwhoooshspeed = 0

func restart_ball():
	ballwhoooshspeed = 600
	randomize()
	ballwhoooshvelocity.x = [-1,1][randi() % 2]
	ballwhoooshvelocity.y = [-0.8,0.8][randi() % 2]
